import * as THREE from "https://cdnjs.cloudflare.com/ajax/libs/three.js/r128/three.module.js";
//import PointerLockControls from "./controls.js";

/*const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.01, 1000);
window.camera = camera;
window.scene = scene;
//const controls = new PointerLockControls(camera, document.body);

const renderer = new THREE.WebGLRenderer();
renderer.domElement.width = window.innerWidth;
renderer.domElement.height = window.innerHeight;
document.body.appendChild(renderer.domElement);
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setClearColor(0xffffff);
window.renderer = renderer;

const box_geom = new THREE.BoxGeometry(1, 1, 1);
const colorless_mat = new THREE.MeshBasicMaterial({ color: 0xff0000 });
const cube = new THREE.Mesh(box_geom, colorless_mat);
scene.add(cube);*/

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

var geometry = new THREE.BoxGeometry( 1, 1, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
//var material = new THREE.MeshNormalMaterial();
var cube = new THREE.Mesh( geometry, material );
scene.add( cube );

function render() {
	//controls.update();
	requestAnimationFrame(render);
	renderer.render(scene, camera);
}
render();

/*const particles = [];
function resize_particles(scale) {
	for (const particle of particles) particle.scale.set(scale, scale, scale);
}
window.resize_particles = resize_particles;

/*fetch("./data.csv").then(res => res.text()).then(dat => {
	const rows = dat.split("\n")
	rows.shift();
	for (const row of rows) {
		const cells = row.split(",");
		if (cells.length !== 4 && cells.length !== 3) { 
			console.error("Skipping line");
			console.error(cells);
		} else {
			//const mat = cells.length === 4 ? new THREE.MeshBasicMaterial({ color: parseFloat(cells[4]) / 14.6793 * 0xffffff }) : colorless_mat;
			const mat = colorless_mat;
			const mesh = new THREE.Mesh(box_geom, mat);
			const scale = 0.1;
			mesh.scale.set(scale, scale, scale);
			mesh.position.set(parseFloat(cells[0]), parseFloat(cells[1]), parseFloat(cells[2]));
			particles.push(mesh);
			scene.add(mesh);
		}
	}
});*/
