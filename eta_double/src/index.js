import * as THREE from "three";
import PointerLockControls from "./controls.js";
import colors_await from "./colors.js";

var scene = new THREE.Scene();
const r = window.innerWidth/window.innerHeight;
let fov = 75;
var camera = new THREE.PerspectiveCamera( 75, r, 0.1, 1000 );
const controls = new PointerLockControls(camera, document.body);
document.body.addEventListener("click", () => { controls.lock(); });

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.setClearColor(0xffffff);
document.body.appendChild( renderer.domElement );
window.scene = scene;
window.camera = camera;
window.renderer = renderer;

const box_geom = new THREE.BoxGeometry(1, 1, 1);
const axis_geom = new THREE.BoxGeometry(0.1, 0.1, 0.5);
const colorless_mat = new THREE.MeshBasicMaterial({ color: 0x000000 });
const cube = new THREE.Mesh(axis_geom, colorless_mat);
scene.add( cube );

//camera.position.z = 5;
camera.position.z = 0;
camera.lookAt(0,0,5);

let forward = false;
let backward = false;
let left = false;
let right = false;
window.addEventListener("keydown", e => {
	if (e.key.toLowerCase() === "w") forward = true;
	if (e.key.toLowerCase() === "s") backward = true;
	if (e.key.toLowerCase() === "a") left = true;
	if (e.key.toLowerCase() === "d") right = true;
	if (e.key === "-" || e.key === "=") {
		fov += e.key === "-" ? 5 : -5;
		camera.fov = fov;
		camera.updateProjectionMatrix();
	}
});
window.addEventListener("keyup", e => {
	if (e.key.toLowerCase() === "w") forward = false;
	if (e.key.toLowerCase() === "s") backward = false;
	if (e.key.toLowerCase() === "a") left = false;
	if (e.key.toLowerCase() === "d") right = false;
});

let prev = 0;
let point = new THREE.Vector3(0,0,-1);
var animate = function (now) {
	const delta = now - prev;

	let delta_x = 0.0, delta_y = 0.0;
	const my_delta_pos = 0.005 * delta;
	if (forward) delta_x = my_delta_pos;
	else if (backward) delta_x = -my_delta_pos;
	if (left) delta_y = -my_delta_pos;
	else if (right) delta_y = my_delta_pos;

	point.set(delta_y, 0, -delta_x);
	point.applyEuler(camera.rotation);
	camera.position.add(point);

	controls.moveForward(delta_x);
	controls.moveRight(delta_y);

	prev = now;
	requestAnimationFrame( animate );

	//cube.rotation.x += 0.01;
	//cube.rotation.y += 0.01;

	//controls.update(delta);
	renderer.render( scene, camera );
};

animate(1);

const particles = [];
function resize_particles(scale) {
	for (const particle of particles) particle.scale.set(scale, scale, scale);
}
window.resize_particles = resize_particles;

const caster = new THREE.Raycaster();

function hslToHex(h, s, l) {
  l /= 100;
  const a = s * Math.min(l, 1 - l) / 100;
  const f = n => {
    const k = (n + h / 30) % 12;
    const color = l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
    return Math.round(255 * color).toString(16).padStart(2, '0');   // convert to Hex and prefix "0" if needed
  };
  return parseInt(`${f(0)}${f(8)}${f(4)}`, 16);
}

function get_mat_energy(energy) {
	return !isNaN(parseFloat(energy)) ? new THREE.MeshBasicMaterial({ color: hslToHex(parseFloat(energy) / 14.6793 * 350, 100, 50) }) : colorless_mat;
}
const cluster_mats = new Map();
function get_mat_cluster(cluster_id) {
	const mat = cluster_mats.get(cluster_id);
	if (mat != null) return mat;
	const new_mat = new THREE.MeshBasicMaterial({ color: hslToHex(Math.random() * 350, 100, 50) });
	cluster_mats.set(cluster_id, new_mat);
	return new_mat;
}
function get_mat_cool(cluster_id) {
	const mat = cluster_mats.get(cluster_id);
	if (mat != null) return mat;
	console.log(window.next_color());
	const new_mat = new THREE.MeshBasicMaterial({ color: parseInt(window["next_color"]().substring(1), 16) });
	cluster_mats.set(cluster_id, new_mat);
	return new_mat;
}
const get_mat = get_mat_cool;

colors_await.then(() => { return fetch(window.location.search.substring(1)) }).then(res => res.text()).then(dat => {
	const rows = dat.split("\n")
	rows.shift();
	for (const row of rows) {
		const cells = row.split(",");
		if (cells.length < 4) { //!== 4 && cells.length !== 3) { 
			console.error("Skipping line");
			console.error(cells);
		} else {
			console.log(parseFloat(cells[3]) / 14.6793 * 0xffffff);
			const mat = get_mat(cells[3]);
			//const mat = colorless_mat;
			const mesh = new THREE.Mesh(box_geom, mat);
			const scale = 0.01;
			mesh.scale.set(scale, scale, scale);
			mesh.position.set(-parseFloat(cells[0]), parseFloat(cells[1]), parseFloat(cells[2]));
			particles.push(mesh);
			scene.add(mesh);
		}
	}
});

window.copy_view = function() {
	console.log(`restore_view(${JSON.stringify(JSON.stringify(camera.position))}, ${JSON.stringify(JSON.stringify(camera.rotation))});`);
}
window.restore_view = function(pos, rot) {
	camera.position.copy(JSON.parse(pos));
	camera.rotation.copy(JSON.parse(rot));
}
