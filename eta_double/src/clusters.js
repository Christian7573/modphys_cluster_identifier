import colors_await from "./colors.js";

function hslToHex(h, s, l) {
  l /= 100;
  const a = s * Math.min(l, 1 - l) / 100;
  const f = n => {
    const k = (n + h / 30) % 12;
    const color = l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
    return Math.round(255 * color).toString(16).padStart(2, '0');   // convert to Hex and prefix "0" if needed
  };
  //return parseInt(`${f(0)}${f(8)}${f(4)}`, 16);
  return `#${f(0)}${f(8)}${f(4)}`;
}
const cluster_mats = new Map();
function get_mat_cluster(cluster_id) {
	const mat = cluster_mats.get(cluster_id);
	if (mat != null) return mat;
	const new_mat = window["next_color"]();
	cluster_mats.set(cluster_id, new_mat);
	return new_mat;
}

const size = 0.1;
const offset = -size / 2;
const size2 = size / 2;
const offset2 = offset / 2;

const stuff = new Map();

const svg_ns = "http://www.w3.org/2000/svg";
const svg = document.querySelector("svg");
colors_await.then(() => { return fetch(window.location.search.substring(1)) })
.then(res => res.text())
.then(dat => {
	let minx = 1000;
	let maxx = -1000;
	let miny = 1000;
	let maxy = -1000;
	window.energy_disp = [];
	for (const line of dat.trim().split("\n")) {
		const bits = line.split(",");
		if (bits.len < 5) continue;
		let stuffz = stuff.get(bits[0]);
		if (stuffz == null) stuffz = [];
		stuff.set(bits[0], stuffz);
		const el = document.createElementNS(svg_ns, "rect");
		const x = parseFloat(bits[2]);
		const y = parseFloat(bits[3]);
		el.setAttribute("width", size);		
		el.setAttribute("height", size);		
		el.setAttribute("fill", get_mat_cluster(bits[0]));
		el.setAttribute("x", x+offset);
		el.setAttribute("y", y+offset);
		minx = Math.min(minx, x);
		maxx = Math.max(maxx, x);
		miny = Math.min(miny, y);
		maxy = Math.max(maxy, y);
		svg.appendChild(el);
		const energy = document.createElementNS(svg_ns, "rect");
		energy.setAttribute("width", size2);
		energy.setAttribute("height", size2);
		energy.setAttribute("x", x+offset2);
		energy.setAttribute("y", y+offset2);
		energy.energy = parseFloat(bits[4]);
		energy_disp.push(energy);
		stuffz.push(energy);
		stuffz.push(el);
	}
	svg.setAttribute("viewBox", `${minx-0.5} ${miny-0.5} ${maxx-minx+1} ${maxy-miny+1}`);
	//svg.setAttribute("viewBox", `${minx-0.5} ${miny-0.5} ${maxx+0.5} ${maxy+0.5}`);
	
	window.max_energy = 0;
	for (const el of energy_disp) {
		max_energy = Math.max(max_energy, el.energy);
		svg.appendChild(el);
	}
	window.actual_max_energy = window.max_energy;
	energy_disp_2(max_energy);
});

window.energy_disp_2 = function(max_energy) {
	window.max_energy = max_energy;
	for (const el of energy_disp) {
		el.setAttribute("fill", `rgba(0,0,0,${Math.min(Math.ceil(el.energy/max_energy*100), 100)}%)`);
	}
}
window.scale = function(x) {
	window.energy_disp_2(actual_max_energy / x);
}
window.disp_cluster = function(x) {
	for (const [cluster, stuffz] of stuff) {
		console.log(cluster);
	}
}
