async function colors() {
	const colors = await (await fetch("/colors.json")).json();
	const mycolors = [];
	for (const yes of colors) { mycolors.push(yes.hex); }
	const di = Math.floor(mycolors.length * 0.65239);
	let i = di;
	window.next_color = function() {
		const ret = mycolors[i];
		mycolors.splice(i, 1);
		i = (i + di) % mycolors.length;
		return ret;
	};
}
const result = colors();
export default result;
