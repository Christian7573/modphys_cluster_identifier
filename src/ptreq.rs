pub struct PtrEq<'a, T>(pub &'a T);
impl<'a, T> PartialEq for PtrEq<'a, T> {
    fn eq(&self, other: &Self) -> bool { std::ptr::eq(self.0, other.0) }
}
impl<'a, T> Eq for PtrEq<'a, T> {}
impl<'a, T> PartialOrd for PtrEq<'a, T> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> { Some(self.cmp(other)) }
}
impl<'a, T> Ord for PtrEq<'a, T> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering { (self.0 as *const T as usize).cmp(&(other.0 as *const T as usize)) }
}
impl<'a, T> Clone for PtrEq<'a, T> { fn clone(&self) -> Self { PtrEq(self.0) } }
impl<'a, T> Copy for PtrEq<'a, T> { }
impl<'a, T> std::ops::Deref for PtrEq<'a, T> {
    type Target = T;
    fn deref(&self) -> &T { self.0 }
}
