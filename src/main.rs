use std::fs::File;
use std::io::{Read, Write};
use std::collections::BTreeMap;

//EventStruct
struct Event<'d> {
    x: f64,
    y: f64,
    z: f64,
    energy: f64,
    tag: Option<&'d str>,

    physphi: f64,
    physeta: f64,
    physdistance: f64,
}

//Tower/Cluster struct
#[derive(Clone)]
struct Tower<'e, 'd> {
    events: Vec<PtrEq<'e, Event<'d>>>,
    energy_sum: f64,
    average_physphi: f64,
    average_physeta: f64,
}

//Pointer Equality Wrapper
mod ptreq;
use ptreq::*;

fn main() {
    process("UnparticleEvent1");
    process("UnparticleEvent2");
}

fn process(file_prefix: &str) {
    let mut dat = String::new();
    let mut file = File::open(format!("{}.csv", file_prefix)).unwrap();
    file.read_to_string(&mut dat).unwrap();        

    //Parse events
    let events = dat.trim().lines().filter_map(|line| {
        let mut stuff = line.split(",");
        let x: f64 = stuff.next()?.parse().ok()?;
        let y: f64 = stuff.next()?.parse().ok()?;
        let z: f64 = stuff.next()?.parse().ok()?;
        //Force parse energy, anything without is a tracking data point
        let energy = stuff.next()?.parse().ok()?;
        let tag = stuff.next();

        //Phi and Eta
        let physphi = y.atan2(x);
        let phystheta = ((x*x) + (y*y)).sqrt().atan2(z);
        //Something something spherical coordinates restrictions
        if phystheta > std::f64::consts::PI || phystheta < 0.0 { panic!("{} {}", phystheta, line) }
        let physeta = -((phystheta / 2.0).tan().ln());
        //Distance from origin to point;
        let physdistance = ((x*x)+(y*y)+(z*z)).sqrt();

        Some(Event {
            x, y, z, energy, physphi, physeta, physdistance, tag
        })
    })
    .collect::<Vec<_>>();

    //Recursive function to pull events into towers
    fn mk_cluster<'a, 'b>(max_dphieta: f64, max_ddist: f64, events: &mut Vec<&'a Event<'b>>, event: &'a Event<'b>, out: &mut Vec<&'a Event<'b>>) {
        let mut i = 0;
        //Store events in here until the current event has grabbed all applicable, then recurse
        let mut captured_events = Vec::new();
        out.push(event);
        while i < events.len() {
            let current = events[i];
            let dphi = event.physphi - current.physphi;
            let deta = event.physeta - current.physeta;
            //Distance on the phi-eta plane(?)
            let dphieta = ((dphi*dphi)+(deta*deta)).sqrt();
            if dphieta <= max_dphieta && (event.physdistance - current.physdistance).abs() < max_ddist {
                captured_events.push(events.remove(i));
            } else {
                i += 1;
            }
        }
        for event in captured_events {
            mk_cluster(max_dphieta, max_ddist, events, event, out);
        }
    }

    //Distance on the phi-eta plane(?)
    const MAX_DPHIETA: f64 = 0.03;
    //Difference in distance from  origin
    const MAX_DDISTANCE: f64 = 1.0;

    let events_origional = events;
    let mut events = events_origional.iter().collect::<Vec<_>>();
    let mut towers = Vec::new();
    while !events.is_empty() {
        let mut cluster = Vec::new();
        //Take an event off the events list, assimilate all near it
        let event = events.remove(0);
        mk_cluster(MAX_DPHIETA, MAX_DDISTANCE, &mut events, event, &mut cluster);
        //Calculate some statistics for the tower and add to list
        towers.push(Tower {
            energy_sum: cluster.iter().map(|e| e.energy).sum::<f64>(),
            average_physphi: cluster.iter().map(|e| e.physphi).sum::<f64>() / cluster.len() as f64,
            average_physeta: cluster.iter().map(|e| e.physeta).sum::<f64>() / cluster.len() as f64,
            events: cluster.into_iter().map(|x| PtrEq(x)).collect::<Vec<_>>(),
        });
    }
    println!("{} {} {} {}", towers.len(), towers.iter().map(|x| x.events.len()).sum::<usize>() / towers.len(), towers.iter().map(|x| x.events.len()).max().unwrap_or(0), towers.iter().map(|x| x.events.len()).min().unwrap_or(0));

    //Print out towers for visualizer
    let mut towers_out = File::create(format!("eta_double/src/{}_towers.csv", file_prefix)).unwrap();
    for (i, cluster) in towers.iter().enumerate() {
        for event in &cluster.events {
            writeln!(towers_out, "{},{},{},{}", event.x, event.y, event.z, i).unwrap();
        }
    }

    let max_tower_energy = towers.iter().map(|x| x.energy_sum).sum::<f64>();
    println!("{}", max_tower_energy);

    //Max distance on the phi-eta plane between tower average points for merging
    const MERGER_DPHIETA: f64 = 0.15;
    let origional_towers = towers;
    let mut towers = origional_towers.clone();
    loop {
        println!("Beginning merge with {} towers...", towers.len());
        //Each tower within range of another gets a merge canidate
        //Subsequent canidates found will override those with lesser strength
        struct MergeCanidate<'t, 'e, 'd> {
            tower: &'t Tower<'e, 'd>,
            strength: f64,
        }
        let mut merge_canidates: BTreeMap<PtrEq<Tower>, MergeCanidate> = BTreeMap::new();
        for a in &towers {
            for b in &towers {
                //Don't compare tower to itself
                if PtrEq(a) == PtrEq(b) { continue }
                let dphi = a.average_physphi - b.average_physphi;
                let deta = a.average_physeta - b.average_physeta;
                let dphieta = ((dphi*dphi)+(deta*deta)).sqrt();
                if dphieta <= MERGER_DPHIETA {
                    //Canidate found
                    let canidate = MergeCanidate {
                        tower: a,
                        strength: (MERGER_DPHIETA - dphieta) / MERGER_DPHIETA * a.energy_sum,
                    };
                    if let Some(current_canidate) = merge_canidates.get(&PtrEq(b)) {
                        //Ensure don't die for whatever reason
                        if PtrEq(a) == PtrEq(current_canidate.tower) { panic!("RE"); }
                        if PtrEq(b) == PtrEq(current_canidate.tower) { panic!("RE"); }
                        //Override existing canidate if have enough strength
                        if canidate.strength > current_canidate.strength { merge_canidates.insert(PtrEq(b), canidate); }
                    } else {
                        //This is the only canidate
                        merge_canidates.insert(PtrEq(b), canidate);
                    }
                }
            }
        }
        //If there were no merge canidates, end loop
        if merge_canidates.is_empty() { break }
        println!("Merge canidates: {}", merge_canidates.len());

        //Process canidates
        let mut merge_map: BTreeMap<PtrEq<Tower>, &Tower> = BTreeMap::new();
        for (merging_tower, canidate) in &merge_canidates {
            //If two towers want to merge into eachother, the canidate with higher strength takes
            //presedence. If this is not that canidate, skip it
            let should_merge = merge_canidates.get(&PtrEq(canidate.tower)).map(|inverse_canidate| PtrEq(inverse_canidate.tower) != *merging_tower || canidate.strength > inverse_canidate.strength).unwrap_or(true);
            if should_merge {
                let mut merge_into = merge_map.get(&PtrEq(canidate.tower)).unwrap_or(&canidate.tower);
                //Follow merge canidate chain until reaching a dominate tower
                while let Some(canidate) = merge_map.get(&PtrEq(merge_into)) { merge_into = canidate; }
                merge_map.insert(*merging_tower, merge_into);
            }
        }
        println!("{}", merge_map.len());

        //Actually merge the towers
        let mut new_towers: BTreeMap<PtrEq<Tower>, Tower> = BTreeMap::new();
        for (source_tower, mut dest_tower) in &merge_map {
            //Follow merge canidate chain until reaching a dominate tower
            while let Some(dest) = merge_map.get(&PtrEq(dest_tower)) { dest_tower = dest; }
            let dest = if let Some(t) = new_towers.get_mut(&PtrEq(dest_tower)) { t }
            else {
                //If a clone of the dominate tower hasn't been created, make one
                new_towers.insert(PtrEq(dest_tower), (*dest_tower).clone());
                new_towers.get_mut(&PtrEq(dest_tower)).unwrap()
            };
            //Merge events
            dest.events.append(&mut source_tower.events.clone());
        }
        //Carry over any towers not involved in merging
        let mut carried_over = 0;
        for tower in &towers {
            if merge_canidates.get(&PtrEq(tower)).is_none() {
                new_towers.insert(PtrEq(tower), tower.clone());
                carried_over += 1;
            }
        }
        println!("carried over {}", carried_over);

        //Recalculate tower statistics
        for tower in new_towers.values_mut() {
            tower.energy_sum = tower.events.iter().map(|x| x.energy).sum::<f64>();
            tower.average_physphi = tower.events.iter().map(|x| x.physphi).sum::<f64>() / tower.events.len() as f64;
            tower.average_physeta = tower.events.iter().map(|x| x.physeta).sum::<f64>() / tower.events.len() as f64;
        }
        //Replace old towers list
        towers = new_towers.into_iter().map(|(_, b)| b).collect();
    }

    //Output clusters for 3d visualizer
    let mut clusters_out = File::create(format!("eta_double/src/{}_clusters.csv", file_prefix)).unwrap();
    for (i, cluster) in towers.iter().enumerate() {
        for event in &cluster.events {
            writeln!(clusters_out, "{},{},{},{}", event.x, event.y, event.z, i).unwrap();
        }
    }
    //Output clusters for 2d visualizer
    let mut physclusters_out = File::create(format!("eta_double/src/{}_physclusters.csv", file_prefix)).unwrap();
    for (i, cluster) in towers.iter().enumerate() {
        for event in &cluster.events {
            writeln!(physclusters_out, "{},{},{},{},{}", i, event.physdistance, event.physphi, event.physeta, event.energy).unwrap();
        }
    }
}
